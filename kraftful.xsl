<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <h1>Product report</h1>
                <p>Total count of apps: <xsl:value-of select="count(//app)"/></p>
                <p>Total count of categories: <xsl:value-of select="count(//app/@category)"/></p>
                <p>Total count of price: <xsl:value-of select="sum(//app/@price)"/></p>
                <table>
                    <thead>
                        <tr><th>Price</th><th>Category</th><th>Name</th></tr>
                    </thead>
                    <tbody>
                        <xsl:apply-templates select="//app"/>
                    </tbody>
                </table>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="app">
        <tr>
            <td><xsl:value-of select="@price"/></td>
            <td><xsl:value-of select="@category"/></td>
            <td><xsl:value-of select="@name"/></td>
        </tr>
    </xsl:template>
</xsl:stylesheet>